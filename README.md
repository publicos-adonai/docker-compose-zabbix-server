# Docker Compose Zabbix Server

Projeto para subir um Servidor Zabbix com docker-compose.

Mais detalhes na postagem do meu site:
[Site do Adonai - Ativar Servidor Zabbix 6.4 com Docker Compose](https://www.adonai.eti.br/2023/01/ativar-servidor-zabbix-6-4-com-docker-compose/)
